library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

 entity encode_driver is
 port(
		CLK	:		in std_logic;
		
		tx_busy		: in std_logic;
		
		tx_data		: out std_logic_vector(15 downto 0);
		tx_csw		: out std_logic;
		tx_dw			: out std_logic
		
 );
 
 end entity;
 
 architecture fun of encode_driver is
 
 signal State : std_logic_vector(3 downto 0);
 
 begin
	
	process(CLK, tx_busy)
		begin
			if CLK 'event and CLK = '1' then
				case State is
					when "0000" =>
						tx_csw <= '0';
						tx_dw <= '0';
						tx_data <= (others => '0');
						if tx_busy = '1' then 
							State <= "0000";
						else
							State <= "0001";
						end if;
					when "0001" =>
						tx_csw <= '1';
						tx_data <= X"55AA";
						State <= "0010";
					when "0010" =>
						State <= "0011";
					when "0011" =>
						if tx_busy = '1' then
							tx_csw <= '0';
							tx_data <= (others => '0');
						else
							State <= "0100";
						end if;	
					when "0100" =>	
						tx_dw <= '1';
						tx_data <= X"AA55";
						State <= "0101";							
					when "0101" =>
						State <= "0110";
					when "0110" =>
						if tx_busy = '1' then 
							tx_dw <= '0';
							tx_data <= (others => '0');
						else
							State <= "0111";
						end if;
					when "0111" =>
						tx_csw <= '1';
						tx_data <= X"55AA";
						State <= "1000";
					when "1000" =>
						State <= "1001";
					when "1001" =>
						if tx_busy = '1' then 
							tx_csw <= '0';
							tx_data <= (others => '0');
						else
							State <= "1010";
						end if;
					when "1010" =>
						tx_dw <= '1';
						tx_csw <= '0';
						tx_data <= X"AA55";
						State <= "1011";	
					
					when "1011" =>
						State <= "1100";
					when "1100" =>
						if tx_busy = '1' then 
							tx_dw <= '0';
							tx_data <= (others => '0');
						else
							State <= "1101";
						end if;
					when "1101" =>
						State <= "0000";
					when others =>
						State <= "0000";
				end case;
			end if;
		end process;
end fun;
		
						
							
	
	
	
	
	
	
	
	
	
	
	