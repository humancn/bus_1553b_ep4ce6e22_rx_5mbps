library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

entity CLK_2MHz is
port(
	CLK_200M		: in std_logic;
	
	CLK2M		: out std_logic
);
end entity ;

architecture fun of Clk_2MHz is

signal div_cnt : std_logic_vector(7 downto 0);
signal CLK_BUF	: std_logic;
begin
	process(CLK_200M)
		begin
			if CLK_200M 'event and CLK_200M = '1' then
				if div_cnt = "00110001" then
					div_cnt <= (others => '0');
					CLK_BUF <= not CLK_BUF;
				else
					div_cnt <= div_cnt + 1;
				end if;	
			end if;
		end process;
			
	process(CLK_200M)
		begin
			if CLK_200M 'event and CLK_200M = '1' then
				CLK2M <= CLK_BUF;
			end if;
		end process;
		
		
end fun;
